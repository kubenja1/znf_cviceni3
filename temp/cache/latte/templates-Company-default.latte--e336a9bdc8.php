<?php
// source: /home/users/k/kubenja1/znf/cviceni03b/app/presenters/templates/Company/default.latte

use Latte\Runtime as LR;

class Templatee336a9bdc8 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['c'])) trigger_error('Variable $c overwritten in foreach on line 20');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

<h1>Firmy</h1>

<p>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Pid:default")) ?>">Rodná čísla</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Employer:default")) ?>">Zaměstnanci</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Statistic:default")) ?>">Statistika</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Homepage:default")) ?>">Menu</a>
</p>
<a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("add")) ?>">Vytvoř</a>
<table border="1" width="100%">
    <tr>
        <th>Jméno</th>
        <th>Registrace</th>
        <th>Je plátce DPH?</th>
        <th>Telefon</th>
        <th colspan="2">Akce</th>
    </tr>
<?php
		$iterations = 0;
		foreach ($companies as $c) {
?>
            <tr>
                <td><?php echo LR\Filters::escapeHtmlText($c->name) /* line 22 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->date, $c->registered, "%d.%m.%Y - %H:%M:%S")) /* line 23 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($c->is_dph ? "ANO" : "NE") /* line 24 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->phone, $c->phone)) /* line 25 */ ?></td>
                <td><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("edit", ['id' => $c->id])) ?>">Edituj</a></td>
                <td><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("delete", ['id' => $c->id])) ?>">Odeber</a></td>
            </tr>
<?php
			$iterations++;
		}
?>
    </div>
</table><?php
	}

}
