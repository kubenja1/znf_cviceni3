<?php
// source: /home/user/Documents/znf/cvic3/app/presenters/templates/Company/default.latte

use Latte\Runtime as LR;

class Template03a9225f49 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['c'])) trigger_error('Variable $c overwritten in foreach on line 22');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

<h1>Firmy</h1>

<p>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Pid:default")) ?>">Rodná čísla</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Employer:default")) ?>">Zaměstnanci</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Statistic:default")) ?>">Statistika</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Homepage:default")) ?>">Menu</a>
</p>
<a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("add")) ?>">Vytvoř</a>
<div class="almostTable">
    <header class="th col6">
        <span>Jméno</span>
        <span>Registrace</span>
        <span>Je plátce DPH?</span>
        <span>Telefon</span>
        <span class="doubleCol"><span>Akce</span></span>
        <span></span>
    </header>
    <main class="col6">
<?php
		$iterations = 0;
		foreach ($companies as $c) {
?>
            <section>
                <span><?php echo LR\Filters::escapeHtmlText($c->name) /* line 24 */ ?></span>
                <span><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->date, $c->registered, "%d.%m.%Y - %H:%M:%S")) /* line 25 */ ?></span>
                <span><?php echo LR\Filters::escapeHtmlText($c->is_dph ? "ANO" : "NE") /* line 26 */ ?></span>
                <span><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->phone, $c->phone)) /* line 27 */ ?></span>
                <span><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("edit", ['id' => $c->id])) ?>">Edituj</a></span>
                <span><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("delete", ['id' => $c->id])) ?>">Odeber</a></span>
            </section>
<?php
			$iterations++;
		}
?>
    </main>
    <!-- Taken and modified from https://stackoverflow.com/questions/22036460/creating-html-table-without-using-a-table-tag -->
</div><?php
	}

}
