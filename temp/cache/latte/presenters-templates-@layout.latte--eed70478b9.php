<?php
// source: /home/user/Documents/znf/cvic3/app/presenters/templates/@layout.latte

use Latte\Runtime as LR;

class Templateeed70478b9 extends Latte\Runtime\Template
{
	public $blocks = [
		'css' => 'blockCss',
	];

	public $blockTypes = [
		'css' => 'html',
	];


	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php
		if (isset($this->blockQueue["title"])) {
			$this->renderBlock('title', $this->params, function ($s, $type) {
				$_fi = new LR\FilterInfo($type);
				return LR\Filters::convertTo($_fi, 'html', $this->filters->filterContent('striphtml', $_fi, $s));
			});
			?> | <?php
		}
?>BI-ZNF</title>

		<style>
			div.almostTable {
				display: table;
				width: 100%;
				border: 1px solid black;
			}

			div.almostTable header, div.almostTable main section {
				display: table-row;
				border: 1px solid black;
			}

			.almostTable > main { display: table-row-group }

			.almostTable > .th {
				text-align: center;
				font-weight: bold;
			}

			.almostTable > .col6 span {
				display: table-cell;
				width: 16.5%;
				border: 1px solid black;
			}

			.almostTable > .col6 span.doubleCol {
				position: relative;
				border-right: none;
			}
			.almostTable > .col6 span.doubleCol + span {
				border-left: none;
			}
			.almostTable > .col6 span.doubleCol > span {
				position:absolute;
				border: none;
				top: 0;
				left: 0;
				width: 200%;
			}
			/* Taken and modified from https://stackoverflow.com/questions/22036460/creating-html-table-without-using-a-table-tag */

		</style>
<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('css', get_defined_vars());
?>
	</head>

	<body>
<?php
		$iterations = 0;
		foreach ($flashes as $flash) {
			?>		<div<?php if ($_tmp = array_filter(['flash', $flash->type])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>><?php
			echo LR\Filters::escapeHtmlText($flash->message) /* line 56 */ ?></div>
<?php
			$iterations++;
		}
?>

<?php
		$this->renderBlock('content', $this->params, 'html');
?>
	</body>
</html>
<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['flash'])) trigger_error('Variable $flash overwritten in foreach on line 56');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockCss($_args)
	{
		
	}

}
