<?php
// source: /home/users/k/kubenja1/znf/cviceni03b/app/presenters/templates/Employer/default.latte

use Latte\Runtime as LR;

class Templateb54a4ddec0 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['e'])) trigger_error('Variable $e overwritten in foreach on line 26');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

<h1>Zaměstnanci</h1>

<p>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Pid:default")) ?>">Rodná čísla</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Company:default")) ?>">Firmy</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Statistic:default")) ?>">Statistiky</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Homepage:default")) ?>">Menu</a>
</p>

<a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("add")) ?>">Vytvoř</a>
<table border="1" width="100%">
    <tr>
        <th>Firma</th>
        <th>Jméno</th>
        <th>Příjmení</th>
        <th>Rodné číslo</th>
        <th>Pohlaví</th>
        <th>Datum narození</th>
        <th>Plat</th>
        <th>Daň</th>
        <th colspan="2">Akce</th>

    </tr>
<?php
		$iterations = 0;
		foreach ($employers as $e) {
?>
            <tr>
                <td><?php echo LR\Filters::escapeHtmlText($e->company->name) /* line 28 */ ?> </td>
                <td><a href="http://www.kdejsme.cz/jmeno/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($e->firstname)) /* line 29 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($e->firstname) /* line 29 */ ?></a></td>
                <td><a href="http://www.kdejsme.cz/prijmeni/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($e->surname)) /* line 30 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($e->surname) /* line 30 */ ?></a></td>
                <td><?php echo LR\Filters::escapeHtmlText($e->pid == NULL ? "nezadáno" : $e->pid->name) /* line 31 */ ?></td>
                <td>???</td>
                <td><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->birthday, $e->pid_id)) /* line 33 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($e->salary) /* line 34 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($e->salary * 0.22) /* line 35 */ ?></td>
                <td><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("edit", ['id' => $e->id])) ?>">Edituj</a></td>
                <td><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("delete", ['id' => $e->id])) ?>">Odeber</a></td>
            </tr>
<?php
			$iterations++;
		}
?>
    </div>
</table><?php
	}

}
