<?php

namespace App\Presenters;

use App\Model\EmployerModel;
use App\Model\CompanyModel;
use App\Forms\EmployerFormFactory;
use App\Model\UtilityModel;
use Nette\Application\UI\Form;
use App\Model\NoDataFound;



class EmployerPresenter extends BasePresenter
{
    /** @var EmployerFormFactory - Formulářová továrnička pro správu zaměstanců */
    private $formFactory;

    /** @var EmployerModel - model pro management zaměstanců */
    private $employerModel;

    /** @var CompanyModel - model pro management firem */
    private $companyModel;

    /** @var UtilityModel */
    private $utilityModel;

    /**
     * Setter pro formulářovou továrničku a model správy firem.
     *
     * @param CompanyModel $companyModel
     * @param EmployerModel $employerModel
     * @param EmployerFormFactory $companyFormFactory
     * @param UtilityModel $utilityModel
     */
    public function injectCompanyDependencies(
        CompanyModel $companyModel,
        EmployerModel $employerModel,
        EmployerFormFactory $companyFormFactory,
        UtilityModel $utilityModel)
    {
        $this->companyModel = $companyModel;
        $this->employerModel = $employerModel;
        $this->formFactory = $companyFormFactory;
        $this->utilityModel = $utilityModel;
    }

    /**
     * Akce pro vkádání
     */
    public function actionAdd() {
        $form = $this['addForm'];
        try {
            $companies = $this->companyModel->listCompanies();
            $c = [];
            foreach($companies as $company)
                $c[$company['id']] = $company['name'];
            $form['company_id']->setItems($c);
        } catch (NoDataFound $e) {
            $form->addError('Nelze načíst data');
        }
    }

    /**
     * Akce pro editaci
     * @param int $id id zaměstnance
     */
    public function actionEdit($id) {
        $form = $this['editForm'];
        try {
            $companies = $this->companyModel->listCompanies();
            $c = [];
            foreach($companies as $company)
                $c[$company['id']] = $company['name'];
            $form['company_id']->setItems($c);
            $employer = $this->employerModel->getEmployer($id);
            $form->setDefaults($employer);
        } catch (NoDataFound $e) {
            $form->addError('Nelze načíst data');
        }
    }

    /**
     * Akce pro mazání
     * @param int $id id zaměstnance
     */
    public function actionDelete($id) {
        $form = $this['deleteForm'];
        $form['id']->setDefaultValue($id);
    }

    /**
     * Metoda pro vytvoření formuláře pro vložení
     * @return Form - formulář
     */
    public function createComponentAddForm()
    {
        $form = $this->formFactory->createAddForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuáře pro editaci
     * @return Form - formulář
     */
    public function createComponentEditForm()
    {
        $form = $this->formFactory->createEditForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuláře pro mazání
     * @return Form - formulář
     */
    public function createComponentDeleteForm()
    {
        $form = $this->formFactory->createDeleteForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderEdit($id) {
        $employer = $this->employerModel->getEmployer($id);
        $this->template->name = $employer['surname'];
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDelete($id) {
        $employer = $this->employerModel->getEmployer($id);
        $this->template->name = $employer['surname'];
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault() {
       $this->template->employers = $this->employerModel->listEmployers();

       $this->template->addFilter('sex', function ($s) {
            switch(intval($this->utilityModel->isMan($s)))
            {
                case -1:
                    return "NEDEF";
                case 0:
                    return "ŽENY";
                case 1:
                    return "MUŽ";
            }
       });

       $this->template->addFilter('birthday', function ($s) {
            $birtday = $this->utilityModel->getBirthDay($s);

            if ($birtday == NULL)
                return "!!" . $s;
            return $birtday->format("d.m.Y");
        });
    }
}
