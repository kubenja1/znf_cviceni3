<?php

namespace App\Model;

use Nette\Neon\Exception;
use Nette\Utils\DateTime;

class UtilityModel extends BaseModel
{
    /** @var PidModel - model pro management rc*/
    private $pidModel;

    /**
     * Setter pro formulářovou továrničku a model správy firem.
     *
     * @param PidModel $pidModel
     */
    public function injectDependencies(PidModel $pidModel)
    {
        $this->pidModel = $pidModel;
    }

    /**
     * Metoda detekuje pohlaví -1 = nedefinováno, 0 - žena, 1 - muž
     * @param int  $id rodného čísla
     */
    public function isMan($id)
    {
        if(!$id) return -1;
        $pid = $this->pidModel->getPid($id);
        if(!$pid) return -1;
        $rc = substr($pid['name'],2,2);
        return $rc<50;
    }

    /**
     * Metoda detekuje datum narození
     * @param int  $id rodného čísla
     */
    public function getBirthDay($id)
    {
        $rc = $this->pidModel->getPid($id);
        if ($rc == NULL) {
            return NULL;
        }
        return $this->checkRC($rc->name);
    }

    /**
     * Taken from https://phpfashion.com/jak-overit-platne-ic-a-rodne-cislo
     */
    private function checkRC($rc)
    {
        // be liberal in what you receive
        if (!preg_match('#^\s*(\d\d)(\d\d)(\d\d)[ /]*(\d\d\d)(\d?)\s*$#', $rc, $matches)) {

            return FALSE;
        }

        list(, $year, $month, $day, $ext, $c) = $matches;

        // k měsíci může být připočteno 20, 50 nebo 70
        if ($month > 70 && $year > 2003) {
            $month -= 70;
        } elseif ($month > 50) {
            $month -= 50;
        } elseif ($month > 20 && $year > 2003) {
            $month -= 20;
        }

        // kontrola data
        if (!checkdate($month, $day, $year)) {
            return NULL;
        }

        return new DateTime("{$year}-{$month}-{$day}");
    }

}